CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

A module for compressing the images which are already existing in a website
and compressing the images for new uploads.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/image_compression

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/image_compression


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Image Compression module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

  1. Navigate to Administration > Extend and enable the module.
  2. Navigate to Administration > Configuration -> Image Compression
  3. Enter the file size in bytes. If you need to select images of above 100KB
     provide the size value as 100000 and select the compression rate values.
  4. Try to upload the image and the image gets comperssed.
  5. To compress existing images, navigate to Administration > Configuration >
     Compress Existing Images, and perform the operation.

MAINTAINERS
-----------

 * Mithun S - https://www.drupal.org/u/mithun-s

Supporting organizations:

 * Specbee - https://www.drupal.org/specbee
