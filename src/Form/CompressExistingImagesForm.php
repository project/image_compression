<?php

namespace Drupal\image_compression\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\image_compression\ImageCompressionHelperService;
use Drupal\image_compression\ImageCompressionServices;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Image Compression existing images form.
 */
class CompressExistingImagesForm extends ConfigFormBase {

  /**
   * Image compression helper service.
   *
   * @var \Drupal\image_compression\ImageCompressionHelperService
   */
  protected $icHelper;

  /**
   * Image compression service.
   *
   * @var \Drupal\image_compression\ImageCompressionServices
   */
  protected $icService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('image_compression.helper'),
      $container->get('image_compression.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ImageCompressionHelperService $ic_helper, ImageCompressionServices $ic_service) {
    parent::__construct($config_factory);
    $this->icHelper = $ic_helper;
    $this->icService = $ic_service;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'existing_image';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'image_compression.compress',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $options = $this->icHelper::getAllOptions();

    $help = [
      $this->t('Use this page to apply the current compression settings to existing images.'),
    ];

    foreach ($options as $option) {
      $help[] = "&nbsp;&nbsp;- " . $this->t('Files equal or over @size will be compressed at a rate of @rate', [
        '@rate' => $option['rate'] . '%',
        '@size' => $this->icHelper::bytesToHumanReadableSize($option['size']),
      ]);
    }

    $help[] = "<br><strong>" . $this->t('WARNING: Applying these changes can not be undone and should only be used if know what you are doing.') . '</strong><br><br>';

    $form['help'] = [
      '#type' => 'item',
      '#title' => $this->t('Compress Existing Images'),
      '#markup' => implode("<br/>", $help),
    ];

    $form['actions']['submit']['#value'] = $this->t('Compress Existing Images');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->icService->compressImages();
  }

}
