<?php

namespace Drupal\image_compression\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image_compression\ImageCompressionHelperService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Image Compression settings form.
 */
class ImageCompressionSettingsForm extends ConfigFormBase {

  /**
   * Image compression helper service.
   *
   * @var \Drupal\image_compression\ImageCompressionHelperService
   */
  protected $icHelper;

  /**
   * Current number.
   *
   * @var int
   */
  protected $number = 1;

  /**
   * Array storing the temporary removed items.
   *
   * @var array
   */
  protected $tempRemoved = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('image_compression.helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ImageCompressionHelperService $ic_helper) {
    parent::__construct($config_factory);
    $config = $this->config('image_compression.settings');
    $this->icHelper = $ic_helper;
    $this->number = $config->get($this->icHelper::COUNT_FIELD) ?? 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'form_compression';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'image_compression.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('image_compression.settings');
    $form['#tree'] = TRUE;

    $help = [
      $this->t('To use this Image compression module, there are two steps:'),
      '&nbsp;&nbsp;- ' . $this->t('Set an image size, this should be a number followed by file prefix e.g. kb, mb, gb.'),
      '&nbsp;&nbsp;- ' . $this->t('Set the compression rate if a file size is larger.'),
    ];
    $form['help'] = [
      '#type' => 'item',
      '#title' => $this->t('Image Compression Settings'),
      '#markup' => implode("<br/>", $help),
    ];

    $header = [
      'size' => $this->t('Image size'),
      'rate' => $this->t('Compression rate'),
      'remove' => '',
    ];
    $form['compress']['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#attributes' => ['id' => 'compressor'],
    ];

    for ($i = 1; $i <= $this->number; $i++) {
      if (!in_array($i, $this->tempRemoved)) {
        // Image size field.
        $form['compress']['table'][$i][$this->icHelper::SIZE_FIELD] = [
          '#type' => 'textfield',
          '#title' => $this->t('Image size'),
          '#title_display' => 'invisible',
          '#default_value' => $this->icHelper::bytesToHumanReadableSize($config->get($this->icHelper::createConfigNameSize($i))),
          '#size' => 20,
        ];

        $rates = $this->rateOptions();
        // Compression rate field.
        $form['compress']['table'][$i][$this->icHelper::RATE_FIELD] = [
          '#type' => 'select',
          '#options' => $rates,
          '#title' => $this->t('Compression rate'),
          '#title_display' => 'invisible',
          '#default_value' => $config->get($this->icHelper::createConfigNameRate($i)),
        ];

        // Remove button.
        $form['compress']['table'][$i]['remove'] = [
          '#type' => 'submit',
          '#value' => $this->t('Remove'),
          '#data' => $i,
          '#name' => 'compress-table-' . $i . '-remove',
          '#submit' => ['::imageCompressionRemoveItem'],
          '#ajax' => [
            'callback' => '::imageCompressionAjaxCallback',
            'wrapper' => 'compressor',
          ],
        ];
      }
    }

    $num = $this->number + 1;
    $form['compress']['table'][$num] = [
      'add_item' => [
        '#type' => 'submit',
        '#value' => $this->t('Add option'),
        '#submit' => ['::imageCompressionAddItem'],
        '#ajax' => [
          'callback' => '::imageCompressionAjaxCallback',
          'wrapper' => 'compressor',
        ],
      ],
      'nothing-1' => [],
      'nothing-2' => [],
    ];

    $form['compress_before'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Compress before upload'),
      '#default_value' => $config->get('compress_before'),
      '#description' => $this->t('When enabled compression shall happen before size validation happens.'),
    ];

    return $form;
  }

  /**
   * Generate rate options.
   */
  protected function rateOptions(): array {
    $options = [];
    for ($i = 0; $i < 100; $i += 5) {
      if ($i === 0) {
        continue;
      }
      $options[$i] = $i . '%';
    }
    return $options;
  }

  /**
   * Ajax callback returns form method.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function imageCompressionAjaxCallback(array $form, FormStateInterface $form_state) {
    return $form['compress'];
  }

  /**
   * Ajax callback rebuilds forms.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function imageCompressionAddItem(array &$form, FormStateInterface $form_state) {
    $this->number++;
    $form_state->setRebuild();
  }

  /**
   * Ajax callback rebuilds forms.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function imageCompressionRemoveItem(array &$form, FormStateInterface $form_state) {
    $index = $form_state->getTriggeringElement()['#data'];
    $this->tempRemoved[$index] = $index;
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state_input = $form_state->getUserInput();
    for ($i = 1; $i <= $this->number; $i++) {
      if (!in_array($i, $this->tempRemoved)) {
        $size = $form_state_input['compress']['table'][$i][$this->icHelper::SIZE_FIELD];
        $details = $this->icHelper::getNumberAndPrefix($size);
        if ($details['prefix'] === "" || $details['bytes'] === 0) {
          $form_state->setErrorByName('compress][table][' . $i . '][' . $this->icHelper::SIZE_FIELD,
            $this->t('Please enter a valid file size, e.g. 2mb'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state_input = $form_state->getUserInput();
    $config = $this->config('image_compression.settings');

    for ($i = 1; $i <= $this->number; $i++) {
      $size = $form_state_input['compress']['table'][$i][$this->icHelper::SIZE_FIELD];
      $size = $this->icHelper::humanReadableSizeToBytes($size);

      $config
        ->set($this->icHelper::createConfigNameSize($i), $size)
        ->set($this->icHelper::createConfigNameRate($i),
          $form_state_input['compress']['table'][$i][$this->icHelper::RATE_FIELD]);
    }

    $config->set('compress_before', $form_state_input['compress']['compress_before']);
    $config->set($this->icHelper::COUNT_FIELD, ($this->number - count($this->tempRemoved)));
    $config->save();
  }

}
