<?php

namespace Drupal\image_compression;

/**
 * Image Compression helper Service.
 */
class ImageCompressionHelperService {

  /**
   * Count filed name.
   */
  public const COUNT_FIELD = 'count';

  /**
   * Size field name.
   */
  public const SIZE_FIELD = 'size';

  /**
   * Rate field name.
   */
  public const RATE_FIELD = 'rate';

  /**
   * Settings prefix.
   */
  public const SETTINGS_PREFIX = 'details';

  /**
   * Helps generate config name.
   *
   * @param int $index
   *   The index for the config.
   * @param string $field
   *   The field name.
   *
   * @return string
   *   Returns a config name.
   */
  public static function createConfigName(int $index, string $field): string {
    return self::SETTINGS_PREFIX . '.' . $index . '.' . $field;
  }

  /**
   * Shorter method to create config name for size.
   *
   * @param int $index
   *   The index for the config.
   *
   * @return string
   *   Returns a config name.
   */
  public static function createConfigNameSize(int $index): string {
    return self::createConfigName($index, self::SIZE_FIELD);
  }

  /**
   * Shorter method to create config name for rate.
   *
   * @param int $index
   *   The index for the config.
   *
   * @return string
   *   Returns a config name.
   */
  public static function createConfigNameRate(int $index): string {
    return self::createConfigName($index, self::RATE_FIELD);
  }

  /**
   * Converts human-readable file size to bytes.
   *
   * @param string|null $value
   *   Human-readable string.
   *
   * @return int
   *   Bytes.
   */
  public static function humanReadableSizeToBytes(?string $value): int {
    if (!$value) {
      $value = 0;
    }

    $details = self::getNumberAndPrefix($value);

    $bytes = $details['bytes'];
    $prefix = $details['prefix'];

    switch ($prefix) {
      case 't':
        $bytes *= 1024;
        // No break.
      case 'g':
        $bytes *= 1024;
        // No break.
      case 'm':
        $bytes *= 1024;
        // No break.
      case 'k':
        $bytes *= 1024;
        // No break.
    }

    return $bytes;
  }

  /**
   * Converts bytes file size to human-readable.
   *
   * @param string|null $number
   *   Bytes.
   *
   * @return string
   *   Human-readable string.
   */
  public static function bytesToHumanReadableSize(?string $number) : string {
    if (!$number) {
      $number = 0;
    }

    if ($number == 0) {
      return "0KB";
    }

    $s = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
    $e = floor(log($number, 1024));

    return round($number / pow(1024, $e), 2) . $s[$e];
  }

  /**
   * Returns the number and prefix.
   *
   * @param string|null $value
   *   The file size we are parsing.
   *
   * @return array
   *   Returns the bytes and prefix.
   */
  public static function getNumberAndPrefix(?string $value): array {
    if (!$value) {
      $value = "";
    }

    $matches = [];
    preg_match('/^\s*(?P<number>\d+)\s*(?:(?P<prefix>[kmgt]?)b?)?\s*$/i',
      $value, $matches);

    $bytes = intval($matches['number'] ?? 0);
    $prefix = strtolower(strval($matches['prefix'] ?? ''));

    return ['bytes' => $bytes, 'prefix' => $prefix];
  }

  /**
   * Get all the current options.
   *
   * @return array
   *   Returns the array of current options.
   */
  public static function getAllOptions(): array {
    $output = [];
    $config = \Drupal::config('image_compression.settings');
    $count = $config->get(self::COUNT_FIELD) ?? 1;

    for ($i = 1; $i <= $count; $i++) {
      $size = $config->get(self::createConfigNameSize($i));
      $rate = $config->get(self::createConfigNameRate($i));
      $output[$i] = [
        'size' => $size,
        'rate' => $rate,
      ];
    }

    // Highest file sizes first.
    usort($output, function ($item1, $item2) {
      return $item2['size'] <=> $item1['size'];
    });

    return $output;
  }

}
