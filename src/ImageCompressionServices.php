<?php

namespace Drupal\image_compression;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\FileInterface;

/**
 * Image Compression Service.
 */
class ImageCompressionServices {
  use StringTranslationTrait;

  /**
   * The config factory object.
   *
   * @var \Drupal\image_compression\ImageCompressionHelperService
   */
  protected static $icHelper;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected static $configFactory;

  /**
   * Constructs a ServiceExample object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ImageCompressionHelperService $ic_helper) {
    self::$configFactory = $config_factory;
    self::$icHelper = $ic_helper;
  }

  /**
   * Function to get all the files inside a directory and subdirectory.
   *
   * @param string $dir
   *   Path to the files' directory.
   *
   * @return array
   *   Returns the array of files.
   */
  public function getDirContents(string $dir): array {
    $res = [];
    $results = [];
    $item = new \RecursiveDirectoryIterator($dir);
    foreach (new \RecursiveIteratorIterator($item) as $filename => $file) {
      $results[] = $filename;
    }
    foreach ($results as $value) {
      if (filesize($value) == 0) {
        continue;
      }
      if ($value != "." && $value != "..") {
        $res[] = $value;
      }
    }
    return preg_grep('~\.(jpeg|jpg|png)$~', $res);
  }

  /**
   * Function which implements compressImages().
   */
  public function compressImages() {
    $dir = 'sites/default/files';
    $file = $this->getDirContents($dir);
    $batch = [
      'title' => $this->t("Image Compression"),
      'operations' => [
        [
          '\Drupal\image_compression\ImageCompressionServices::imageCompressionBatchProcessing',
          [$file],
        ],
      ],
      'finished' => '\Drupal\image_compression\ImageCompressionServices::imageCompressionBatchProcessFinished',
    ];
    batch_set($batch);
  }

  /**
   * Function to compress png images.
   *
   * @param string $src
   *   The source path to the file.
   * @param int $quality
   *   The quality for the file.
   *
   * @return string
   *   Returns a compressed file.
   */
  public function compressPng(string $src, int $quality): string {
    $source = imagecreatefrompng($src);
    $destination = $src;
    imagepng($source, $destination, $quality);
    clearstatcache();
    return $destination;
  }

  /**
   * Function to compress jpeg/jpg images.
   *
   * @param string $source
   *   The source path to the file.
   * @param int $quality
   *   The quality for the file.
   *
   * @return string
   *   Returns a compressed file.
   */
  public function compressJpg(string $source, int $quality): string {
    $src = imagecreatefromjpeg($source);
    $dest = $source;
    imagejpeg($src, $dest, $quality);
    clearstatcache();
    return $dest;
  }

  /**
   * Function to compress images.
   *
   * @param \Drupal\file\FileInterface $file
   *   Object to implement compress.
   *
   * @return array
   *   returns array of path and compressed status.
   */
  public function compress(FileInterface $file): array {
    $file_compressed = FALSE;
    $file_size = [];
    $quality = [];
    $serial_data = [];

    $config = self::$configFactory->get('image_compression.settings');
    $i = 1;
    while (($config->get(self::$icHelper::createConfigNameSize($i))) != "") {
      $file_size[$i] = $config->get(self::$icHelper::createConfigNameSize($i));
      $quality[$i] = $config->get(self::$icHelper::createConfigNameRate($i));
      $i++;
    }
    for ($i = 1; $i <= count($file_size); $i++) {
      $serial_data[$file_size[$i]] = $quality[$i];
    }
    rsort($file_size);
    $source = $file->getFileUri();
    $size = filesize($source);
    $info = getimagesize($source);
    switch ($info['mime']) {
      case 'image/jpg':
      case 'image/jpeg':
        for ($i = 0; $i < count($file_size); $i++) {
          $current_value = $file_size[$i];
          if ($size > $current_value) {
            $file_compressed = TRUE;
            $this->compressJpg($source, $serial_data[$current_value]);
          }
        }
        break;

      case 'image/png':
        $file_compressed = TRUE;
        $this->compressPng($source, 9);
        break;
    }

    $file->setSize(filesize($source));
    try {
      $file->save();
    }
    catch (EntityStorageException $e) {
    }

    return ['source' => $source, 'compressed' => $file_compressed];
  }

  /**
   * Function for batch process.
   *
   * @param array $files
   *   Contains all the file name.
   * @param array $context
   *   To store progress result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function imageCompressionBatchProcessing(array $files, array &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['max'] = count($files);
    }
    $limit = 10;
    $sliced_datas = array_slice($files, $context['sandbox']['current_id'],
      $limit);
    foreach ($sliced_datas as $value) {
      $context['sandbox']['progress']++;
      $filename = basename($value);
      $files = \Drupal::entityTypeManager()
        ->getStorage('file')
        ->loadByProperties(['filename' => $filename]);
      $file_obj = reset($files);
      if ($file_obj) {
        (new self(self::$configFactory, self::$icHelper))->compress($file_obj);
      }
    }
    $context['sandbox']['current_id'] += $limit;
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Function for batch process finished.
   *
   * @param int $success
   *   Variable stores the success result.
   * @param array $results
   *   The result of the batch.
   * @param array $op
   *   The kind of operation.
   */
  public static function imageCompressionBatchProcessFinished(int $success, array $results, array $op) {
    if ($success) {
      $message = t("Images processed");
    }
    else {
      $message = t("Image Processing Failed");
    }
    \Drupal::messenger()->addMessage($message);
  }

}
